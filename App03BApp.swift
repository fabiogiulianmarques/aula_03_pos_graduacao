//
//  App03BApp.swift
//  App03B
//
//  Created by IOS SENAC on 14/08/21.
//

import SwiftUI

@main
struct App03BApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

