//

// ContentView.swift

// App3B

//

// Created by Adalto Selau Sparremberger on 14/08/21.

//



import SwiftUI

struct CheckToggleStyle: ToggleStyle {
               func makeBody(configuration: Configuration) -> some View {
                   Button {
                       configuration.isOn.toggle()
                   } label: {
                       Label {
                           configuration.label
                       } icon: {
                           Image(systemName: configuration.isOn ? "checkmark.circle.fill" : "circle")
                               .foregroundColor(configuration.isOn ? .accentColor : .secondary)
                               .accessibility(label: Text(configuration.isOn ? "Checked" : "Unchecked"))
                               .imageScale(.large)
                       }
                   }
                   .buttonStyle(PlainButtonStyle())
               }
           }

struct ContentView: View {

    
   

  @State private var texto = ""

  @State var mostraAlerta = false

  @State var idade = 0
    
  @State private var isOn = false

  var body: some View {

     

    VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 10){
        
        HStack (alignment: .center, spacing: 5){
            Text("SORTEIO Hospedagem na praia").bold()
                
               }
        HStack (alignment: .center, spacing: 5){
            Image("Praia").resizable()
                .frame(width: 100, height:100, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        }
    

        HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 5){
        Text ("Nome")
        TextField("digite nome", text: $texto).border(Color.red)
        Spacer(minLength: 60)
        
        Button(action: {
               self.mostraAlerta = true
             }){
               Text("Participar do Sorteio")
             }
              
           }.padding(10)
           .alert(isPresented: $mostraAlerta){ () -> Alert in

             return Alert(title: Text("Atenção"),

                    message: Text("Boa Sorte \(texto)"),

                    dismissButton: .default( Text("OK") ))

           }

    
        
        HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 5){
                Text ("Endereco")
                TextField("digite endereco", text: $texto).border(Color.red)
                       
        }
        

        HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 5){
         
                Text("Celular: ")

                TextField("WhatsApp", text: $texto).border(Color.red)

                Spacer(minLength: 60)
              }

        
        HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 5){
        
            DatePicker(selection: .constant(Date()), label: { Text("Data Nascimento") })
            
            
        }
        
        
        
        HStack(alignment: .center, spacing: 5){
            Text("Acompanhantes: ")
            
            Stepper(value: $idade, in: 0...10){
                Text("\(idade) Adultos")
            }
        }
        
        HStack(alignment: .center, spacing: 5){
                    Text("Acompanhantes: ")
                    
                    Stepper(value: $idade, in: 0...10){
                        Text("\(idade) Menor de 14")
                        
                    }
                }
                
       
        
        HStack(alignment: .center, spacing: 5){
            
           
        
            Toggle("Aceito Receber Mensagens", isOn: $isOn)
                        .toggleStyle(CheckToggleStyle())

       
        }
                

        HStack(alignment: .center, spacing: 5){
                    
            Menu("Menu FINALIZAR") {
                Text("Confirma")
                Text("Limpa")
                Text("Cancela")
            }

                    
                }
        
        
        
        

     
    }
}

struct Quadradinhos : View {
    var body: some View{
        ZStack(alignment: .center, content: {
            
            
        } )
        .border(Color.black, width: 10)
        .padding(30)
        .background(Color.yellow, alignment: .center)
    }
}

    
    
    
    
    

struct ContentView_Previews: PreviewProvider {

  static var previews: some View {

    ContentView()

  }

 }
    
}
